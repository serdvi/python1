"""Вам дается последовательность целых чисел и вам нужно ее обработать и вывести
на экран сумму первой пятерки чисел из этой последовательности, затем сумму второй пятерки, и т. д.
Но последовательность не дается вам сразу целиком. С течением времени к вам поступают её
последовательные части. Например, сначала первые три элемента, потом следующие шесть, потом следующие два и т. д.
"""
class Buffer:
    def __init__(self):
        # конструктор без аргументов
        self.container = []

    def add(self, *a):
        # добавить следующую часть последовательности
        self.container += a
        
        while len(self.container) >= 5:
            print(sum(self.container[:5]))
            del self.container[:5]
            
    def get_current_part(self):
        # вернуть сохраненные в текущий момент элементы последовательности в порядке, в котором они были добавлены
        return self.container