"""Кодирование осуществляется следующим образом:
s = 'aaaabbсaa' преобразуется в 'a4b2с1a2', то есть группы одинаковых символов 
исходной строки заменяются на этот символ и количество его повторений в этой позиции строки."""
st=input()
sum=1
res=''
if len(st)==1: res=st+'1'
else:
    for k in range(1,len(st)):
        if st[k]!=st[k-1]:
            res+=st[k-1]+str(sum)
            sum=1
        else: sum+=1
        if k==len(st)-1:
            res+=st[k]+str(sum)
print(res)