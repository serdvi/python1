"""prime numbers iterator"""
def primes():
    yield 2
    i = 3
    while True:
        b = True
        for k in range(3, int(i**.5)+1,2):
            if i % k == 0 and i != k:
                b = False
                break
        if b: 
            yield i
        i += 2